#define NOMINMAX

#include <bit>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <tuple>
#include <variant>

#include <gsl/narrow>
#include <gsl/pointers>
#include <gsl/span>

#include <tchar.h>
#include <windows.h>

import SendInput;

namespace {
	LPCTSTR _tcsichr(LPCTSTR p, TCHAR ch) noexcept
	{
		assert(p);
		do {
			if (_totlower(gsl::narrow_cast<TCHAR>(_tcsnextc(p))) == _totlower(ch)) {
				return p;
			}
			p = _tcsinc(p);
		} while (gsl::narrow_cast<TCHAR>(_tcsnextc(p)) != 0);

		return nullptr;
	}

	LPCTSTR _tcsistr(LPCTSTR pStr, LPCTSTR pCharSet) noexcept
	{
		assert(pStr);
		int const nLen{ lstrlen(pCharSet) };
		if (nLen == 0) {
			return pStr;
		}

		LPCTSTR pStart{ pStr };
		while (LPCTSTR const pMatch = _tcsichr(pStart, gsl::narrow_cast<TCHAR>(_tcsnextc(pCharSet)))) {
			if (_tcsnicmp(pMatch, pCharSet, nLen) == 0) {
				return pMatch;
			}
			pStart = _tcsinc(pMatch);
		}

		return nullptr;
	}

	struct SCmd {
		SCmd() noexcept
			: hWndConsole{ GetConsoleWindow() }
			, titleExactCase{ nullptr }
			, titleExactNoCase{ nullptr }
			, titleStartCase{ nullptr }
			, titleStartNoCase{ nullptr }
			, titleCase{ nullptr }
			, titleNoCase{ nullptr }
			, cmd{ eCmd::List }
		{}

		// The console window handle. Ignore this window when enumerating.
		HWND hWndConsole;

		LPCTSTR titleExactCase;
		LPCTSTR titleExactNoCase;
		LPCTSTR titleStartCase;
		LPCTSTR titleStartNoCase;
		LPCTSTR titleCase;
		LPCTSTR titleNoCase;

		enum class eCmd {
			List,
			Minimize,
			Maximize,
			Restore,
			Hide,
			Show,
			Foreground,
			Close,
			Layer,
			SendKeys,
			TopMost,
			NoTopMost
		} cmd;

		// Data for cmd == Layer.
		struct SLayer {
			SLayer() noexcept
				: bOn{ false }
				, rgb{}
				, alpha{}
				, flags{}
			{}
			explicit SLayer(BYTE alpha_) noexcept
				: bOn{ true }
				, rgb{}
				, alpha{ alpha_ }
				, flags{ LWA_ALPHA }
			{}
			explicit SLayer(COLORREF rgb_) noexcept
				: bOn{ true }
				, rgb{ rgb_ }
				, alpha{}
				, flags{ LWA_COLORKEY }
			{}

			bool bOn;
			COLORREF rgb;
			BYTE alpha;
			DWORD flags;
		};

		// Data for cmd == SendKeys.
		struct SSendkeys {
			LPCTSTR keys;
			bool bInvalidString;
		};

		std::variant<SLayer, SSendkeys> data;
	};

	[[noreturn]] void Usage(LPCTSTR strExe)
	{
		TCHAR drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		std::ignore = _tsplitpath_s(strExe, drive, dir, fname, ext);
		[[gsl::suppress(bounds .3)]] std::wcerr << fname << ext << R"(
	-te  <exact title case sensitive>
	-ten <exact title case insensitive>
	-ts  <start of title case sensitive>
	-tsn <start of title case insensitive>
	-t   <part of title case sensitive>
	-tn  <part of title case insensitive>
	-list
	-minimize
	-maximize
	-restore
	-hide
	-show
	-foreground
	-close
	-layer [<alpha>]|[<r> <g> <b>]
	-sendkeys <keys>
	-topmost
	-notopmost
Each title search parameter can only be given once. All 6 title parameters can
be given in parallel.
Provide maximum one command at the end of the command line.
<alpha> is a value in the range [0, 255].
<r>, <g> and <b> are values in the range [0, 255].
<keys> can consist of characters, escaped characters "\c", virtual key code
definitions "{vk}", and lists (...). vk responds the VK_vk virtual key codes
defined in the Platform SDK. The keys are parsed from left to right. For each
key a key down and immediately after that a key up message is sent. Lists
contain characters, escaped characters "\c", and virtual key code definitions
"{vk}". They are parsed from left to right. For each key a key down message is
sent. At the end of the list it is traversed from right to left and for each
key a key up message is generated.
List of known virtual key codes:
)";
		for (auto const& k : CSendInput::GetKeymap()) {
			[[gsl::suppress(lifetime .1)]] std::wcerr << k.name << '\n';
		}
		exit(1);
	}

	bool Foreground(HWND hWnd) noexcept
	{
		const HWND hWndPopup{ GetLastActivePopup(hWnd) };
		if (!SetForegroundWindow(hWnd)) {
			return false;
		}
		if (IsIconic(hWnd)) {
			ShowWindow(hWnd, SW_RESTORE);
		}
		if (hWnd != hWndPopup && !SetForegroundWindow(hWndPopup)) {
			return false;
		}
		return true;
	}

	BOOL SendKeys(HWND hWnd, gsl::strict_not_null<SCmd::SSendkeys*> data)
	{
		// Get process and some more data about the window.
		DWORD processid;
		const DWORD threadid{ GetWindowThreadProcessId(hWnd, &processid) };
		const HKL hkl{ GetKeyboardLayout(threadid) };

		// Parse and construct the input.
		CSendInput si(hkl);
		switch (si.Add(data->keys)) {
		case CSendInput::eError::Success:
			break;
		case CSendInput::eError::Syntax:
			data->bInvalidString = true;
			return FALSE;
		}

		// Bring window to foreground so it gets the keyboard input.
		if (!Foreground(hWnd)) {
			return TRUE;
		}

		// Send the input.
		si.Send();

		return TRUE;
	}

	BOOL CALLBACK wndenumproc(HWND hWnd, LPARAM lParam)
		[[gsl::suppress(type .1)]]
	{
		auto const pFilter{ gsl::not_null{ std::bit_cast<SCmd*>(lParam) } };

		// Ignore the console window.
		if (hWnd == pFilter->hWndConsole) {
			return TRUE;
		}

		// Get window title. Skip windows without title.
		std::array<TCHAR, 1024> buf{};
		if (GetWindowText(hWnd, buf.data(), gsl::narrow<int>(buf.size())) == 0) {
			return TRUE;
		}

		// Check if window matches criteria.
		if (pFilter->titleExactCase || pFilter->titleExactNoCase
			|| pFilter->titleStartCase || pFilter->titleStartNoCase
			|| pFilter->titleCase || pFilter->titleNoCase) {
			if (pFilter->titleExactCase && _tcscmp(buf.data(), pFilter->titleExactCase) != 0) {
				return TRUE;
			}
			if (pFilter->titleExactNoCase && _tcsicmp(buf.data(), pFilter->titleExactNoCase) != 0) {
				return TRUE;
			}
			if (pFilter->titleStartCase && _tcsncmp(buf.data(), pFilter->titleStartCase, _tcslen(pFilter->titleStartCase)) != 0) {
				return TRUE;
			}
			if (pFilter->titleStartNoCase && _tcsnicmp(buf.data(), pFilter->titleStartNoCase, _tcslen(pFilter->titleStartNoCase)) != 0) {
				return TRUE;
			}
			if (pFilter->titleCase && !_tcsstr(buf.data(), pFilter->titleCase)) {
				return TRUE;
			}
			if (pFilter->titleNoCase && !_tcsistr(buf.data(), pFilter->titleNoCase)) {
				return TRUE;
			}
		}

		// Window matches criteria.
		switch (pFilter->cmd) {
		case SCmd::eCmd::List:
			std::wcout << _T('"') << buf.data() << _T("\"\n");
			break;
		case SCmd::eCmd::Minimize: {
			[[maybe_unused]] BOOL const b{ PostMessage(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0) };
			assert(b);
			break;
		}
		case SCmd::eCmd::Maximize: {
			[[maybe_unused]] BOOL const b{ PostMessage(hWnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0) };
			assert(b);
			break;
		}
		case SCmd::eCmd::Restore: {
			[[maybe_unused]] BOOL const b{ PostMessage(hWnd, WM_SYSCOMMAND, SC_RESTORE, 0) };
			assert(b);
			break;
		}
		case SCmd::eCmd::Hide:
			ShowWindow(hWnd, SW_HIDE);
			break;
		case SCmd::eCmd::Show:
			ShowWindow(hWnd, SW_SHOW);
			break;
		case SCmd::eCmd::Foreground:
			Foreground(hWnd);
			break;
		case SCmd::eCmd::Close: {
			[[maybe_unused]] BOOL const b{ PostMessage(hWnd, WM_SYSCOMMAND, SC_CLOSE, 0) };
			assert(b);
			break;
		}
		case SCmd::eCmd::Layer: {
			auto const& data{ std::get<SCmd::SLayer>(pFilter->data) };
			auto dwStyle{ GetWindowLongPtr(hWnd, GWL_EXSTYLE) };
			SetLastError(ERROR_SUCCESS);
			if (data.bOn) {
				dwStyle |= WS_EX_LAYERED;
			}
			else {
				dwStyle &= ~WS_EX_LAYERED;
			}
			dwStyle = SetWindowLongPtr(hWnd, GWL_EXSTYLE, dwStyle);
			assert(dwStyle != 0 || GetLastError() != ERROR_SUCCESS);
			if (data.bOn) {
				[[maybe_unused]] BOOL const b{ SetLayeredWindowAttributes(hWnd, data.rgb, data.alpha, data.flags) };
				assert(b);
			}
			break;
		}
		case SCmd::eCmd::SendKeys:
			return SendKeys(hWnd, gsl::make_strict_not_null(&std::get<SCmd::SSendkeys>(pFilter->data)));
		case SCmd::eCmd::TopMost: {
			constexpr UINT nFlags{ SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER };
			[[maybe_unused]] BOOL const b{ SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, nFlags) };
			assert(b);
			break;
		}
		case SCmd::eCmd::NoTopMost: {
			constexpr UINT nFlags{ SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER };
			[[maybe_unused]] BOOL const b{ SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, nFlags) };
			assert(b);
			break;
		}
		default:
			__assume(false);
			break;
		}

		return TRUE;
	}
}	// namespace

[[gsl::suppress(bounds .3, type .1)]] int _tmain(int argc, _TCHAR* argv[])
{
	std::ios_base::sync_with_stdio(false);

	gsl::span<_TCHAR*, gsl::dynamic_extent> args{ argv, gsl::narrow<size_t>(argc) };

	if (args.size() == 1) {
		Usage(gsl::at(args, 0));
	}

	// Parse parameters.
	SCmd filter;
	for (auto iterArg{ std::begin(args) + 1 }; iterArg != std::end(args); ++iterArg) {
		std::wstring_view const currentArg{ gsl::not_null{ *iterArg } };
		if (currentArg.empty() || (!currentArg.starts_with(_T('-')) && !currentArg.starts_with(_T('/')))) {
			Usage(gsl::at(args, 0));
		}
		static constexpr std::array<std::pair<LPCTSTR, LPCTSTR SCmd::*>, 6> sc_params1{
			// clang-format off
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("te"),  &SCmd::titleExactCase },
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("ten"), &SCmd::titleExactNoCase },
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("ts"),  &SCmd::titleStartCase },
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("tsn"), &SCmd::titleStartNoCase },
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("t"),   &SCmd::titleCase },
			std::pair<LPCTSTR, LPCTSTR SCmd::*>{ _T("tn"),  &SCmd::titleNoCase },
			// clang-format on
		};
		auto const iter1{ std::ranges::find_if(sc_params1, [currentArg](auto const& p) { return _tcsicmp(&currentArg.at(1), p.first) == 0; }) };
		if (iter1 != end(sc_params1)) {
			if (++iterArg == std::end(args)) {
				Usage(gsl::at(args, 0));
			}
			filter.*(iter1->second) = *iterArg;
			continue;
		}

		static const std::array<std::pair<LPCTSTR, SCmd::eCmd>, 10> sc_params2{
			// clang-format off
			std::pair<LPCTSTR, SCmd::eCmd> { _T("list"),		SCmd::eCmd::List },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("minimize"),	SCmd::eCmd::Minimize },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("maximize"),	SCmd::eCmd::Maximize },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("restore"),		SCmd::eCmd::Restore },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("hide"),		SCmd::eCmd::Hide },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("show"),		SCmd::eCmd::Show },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("foreground"),	SCmd::eCmd::Foreground },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("close"),		SCmd::eCmd::Close },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("topmost"),		SCmd::eCmd::TopMost },
			std::pair<LPCTSTR, SCmd::eCmd> { _T("notopmost"),	SCmd::eCmd::NoTopMost },
			// clang-format on
		};
		auto const iter2{ std::ranges::find_if(sc_params2, [currentArg](auto const& p) noexcept { return _tcsicmp(&currentArg.at(1), p.first) == 0; }) };
		if (iter2 != end(sc_params2)) {
			if (iterArg + 1 != std::end(args)) {
				Usage(gsl::at(args, 0));
			}
			filter.cmd = iter2->second;
			break;
		}

		if (_tcsicmp(&currentArg.at(1), _T("layer")) == 0) {
			if (iterArg + 1 == std::end(args)) {
				filter.data = SCmd::SLayer{};
			}
			else if (std::distance(iterArg, std::end(args)) == 1 + 1) {
				//TODO(me): check for byte range
				filter.data = SCmd::SLayer{ gsl::narrow_cast<BYTE>(_ttoi(*++iterArg)) };
			}
			else if (std::distance(iterArg, std::end(args)) == 1 + 3) {
				//TODO(me): check for byte range
				auto const r{ _ttoi(*++iterArg) };
				auto const g{ _ttoi(*++iterArg) };
				auto const b{ _ttoi(*++iterArg) };
				filter.data = SCmd::SLayer{ RGB(r, g, b) };
			}
			else {
				Usage(gsl::at(args, 0));
			}
			filter.cmd = SCmd::eCmd::Layer;
			break;
		}

		if (_tcsicmp(&currentArg.at(1), _T("sendkeys")) == 0) {
			if (std::distance(iterArg, std::end(args)) != 1 + 1) {
				Usage(gsl::at(args, 0));
			}
			filter.cmd	= SCmd::eCmd::SendKeys;
			filter.data = SCmd::SSendkeys{ *++iterArg, false };
			break;
		}

		Usage(gsl::at(args, 0));
	}

	// Do the work.
	BOOL const b{ EnumWindows(wndenumproc, std::bit_cast<LPARAM>(&filter)) };
	if (filter.cmd == SCmd::eCmd::SendKeys && std::get<SCmd::SSendkeys>(filter.data).bInvalidString) {
		Usage(gsl::at(args, 0));
	}

	return b ? 0 : 2;
}
