# wndcmd

This tool finds windows with a given title and sends them commands.

## Syntax
wndcmd <filters> <command>  
where  
<filters> = [-te <exact title case sensitiv>] [-ten <exact titlecase insensitiv>] [-ts <start of title casesensitiv>] [-tsn <start of title case insensitiv>] [-t <part of title case sensitiv>] [-tn<part of title case insensitiv>]  
<command> = (-list | -minimize | -maximize | -restore | -hide | -show | -foreground | -close | -layer ([<alpha>] | [<r> <g><b>]) | -sendkeys <keys> | -topmost | -notopmost)  
<keys> = <char> | '\' <char2> | '{' <vk> '}' | '(' <keylist> ')'  
<keylist> = <char> | '\' <char2> | '{' <vk> '}'  
<char> = any character except '\', '{' and '('  
<char2> = any character  
<vk> = virtual key code as defined in the platform SDK (for example "{SHIFT}" = `VK_SHIFT`)


## Description
The program searches all windows where the title matches the given filter criteria. If no filter was given, then all windows are enumerated.
In every case windows with an empty title are ignored. The given command is applied to all found windows.

 - -list dumps a list of all found windows.
 - -minimize/-maximize minimizes/maximizes the window
 - -restore restores the window
 - -hide hides the window
 - -show shows the window
 - -foreground brings the window into foreground
 - -close sends a close message to the window
 - -layer <alpha> sets the opacity of the window (0 ≤ alpha ≤ 255) where 0 is fully transparent and 255 is fully opaque<br>
 - -layer <r> <g> <b> sets the color with RGB value (r, g, b) to transparent (0 ≤ r/g/b ≤ 255)
 - -layer switches off transparency/opacity
 - -sendkeys brings the window to foreground and sets keyboard input to it (using `SendInput`)
 - -topmost activates the window option 'always in foreground'
 - -notopmost deactivates the window option 'always in foreground'

<keys> is processed from left to right. For each key or character a key down followed by a key up is generated.  
<keylist> is processed from left to right. First a key down is generated for each key or character, then a key up is generated for each key or character in the reverse order of the key down events.

## System requirements
Windows 2000, Windows XP or higher

## Installation
Not necessary. The program can be run without installation.
