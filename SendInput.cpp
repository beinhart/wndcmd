module;

#define NOMINMAX

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <exception>
#include <optional>

#include <gsl/narrow>

#include <tchar.h>
#include <windows.h>

module SendInput;

//TODO(me): Sleep per {<Anzahl ms>}

CSendInput::CSendInput(HKL hkl)
	: m_hkl{ hkl }
{
	m_countKeyDown.fill(0);
}

CSendInput::eError CSendInput::Add(LPCTSTR str)
{
	try {
		return AddStr(str);
	}
	catch (const std::exception&) {
		return eError::Error;
	}
}

void CSendInput::Send() noexcept
{
	// Send the events.
	const UINT nDone{ SendInput(gsl::narrow<UINT>(m_input.size()), m_input.data(), sizeof(decltype(m_input)::value_type)) };
	assert(nDone == m_input.size());
}

CSendInput::eError CSendInput::AddStr(LPCTSTR str)
{
	std::optional<std::size_t> nListStart;	 // Valid if in a list.
	for (;;) {
		switch (TCHAR c{ gsl::narrow_cast<TCHAR>(_tcsnextc(str)) }; c) {
		case _T('\0'):
			// End of string.
			if (nListStart) {
				return eError::Syntax;
			}
			return eError::Success;
		case _T('\\'):
			// Escape character.
			str = _tcsinc(str);
			c	= gsl::narrow_cast<TCHAR>(_tcsnextc(str));
			if (c == _T('\0')) {
				return eError::Syntax;
			}
			AddInputChar(c, true, !nListStart);
			str = _tcsinc(str);
			break;
		case _T('{'):
			// Giving {VK_NAME}.
			str = _tcsinc(str);
			{
				auto const iter{ std::ranges::find_if(GetKeymap(), [str](auto k) noexcept {
					auto const len{ _tcslen(k.name) };
					return _tcsnicmp(str, k.name, len) == 0 && gsl::narrow_cast<TCHAR>(_tcsnextc(_tcsninc(str, len))) == _T('}');
				}) };
				if (iter == end(GetKeymap())) {
					return eError::Syntax;
				}
				AddInputVk(iter->vk, true, !nListStart);
				str = _tcsninc(str, _tcslen(iter->name) + 1);
			}
			break;
		case _T('('):
			// Start of a list. Nested lists are not allowed
			if (nListStart) {
				return eError::Syntax;
			}
			str		   = _tcsinc(str);
			nListStart = m_input.size();
			break;
		case _T(')'):
			// End of a list.
			if (!nListStart.has_value() || m_input.size() == nListStart.value()) {
				return eError::Syntax;
			}
			str = _tcsinc(str);
			for (auto i{ m_input.size() }; i-- > nListStart.value();) {
				// No double key up for one scan code.
				if (--m_countKeyDown.at(m_input.at(i).ki.wVk & 0xFF) == 0) {
					m_input.emplace_back(m_input.at(i)).ki.dwFlags |= KEYEVENTF_KEYUP;
				}
			}
			nListStart.reset();
			break;
		default:
			// Any other character.
			AddInputChar(c, true, !nListStart);
			str = _tcsinc(str);
			break;
		}
	}
}

void CSendInput::AddInputChar(TCHAR c, bool bDown, bool bUp)
{
	const SHORT vk{ VkKeyScanEx(c, m_hkl) };
	if (!(1 <= LOBYTE(vk) && LOBYTE(vk) <= 254)) {
		return;
	}
	AddInputVk(vk, bDown, bUp);
}

void CSendInput::AddInputVk(SHORT vk, bool bDown, bool bUp)
{
	// Get scan code.
	auto const scan{ gsl::narrow_cast<WORD>(MapVirtualKeyEx(vk & 0xff, 0, m_hkl)) };
	if (scan == 0) {
		return;
	}

	if (bDown) {
		if ((vk & 0x0100) != 0) {
			AddInput(VK_SHIFT, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_SHIFT, 0, m_hkl)), true);
		}
		if ((vk & 0x0200) != 0) {
			AddInput(VK_CONTROL, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_CONTROL, 0, m_hkl)), true);
		}
		if ((vk & 0x0400) != 0) {
			AddInput(VK_MENU, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_MENU, 0, m_hkl)), true);
		}
		AddInput(gsl::narrow_cast<WORD>(vk & 0xff), scan, true);
	}
	if (bUp) {
		AddInput(gsl::narrow_cast<WORD>(vk & 0xff), scan, false);
		if ((vk & 0x0400) != 0) {
			AddInput(VK_MENU, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_MENU, 0, m_hkl)), false);
		}
		if ((vk & 0x0200) != 0) {
			AddInput(VK_CONTROL, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_CONTROL, 0, m_hkl)), false);
		}
		if ((vk & 0x0100) != 0) {
			AddInput(VK_SHIFT, gsl::narrow_cast<WORD>(MapVirtualKeyEx(VK_SHIFT, 0, m_hkl)), false);
		}
	}
}

void CSendInput::AddInput(WORD vk, WORD scan, bool bDown)
{
	if (bDown) {
		++m_countKeyDown.at(vk & 0xFF);
	}
	else {
		--m_countKeyDown.at(vk & 0xFF);
		assert(m_countKeyDown.at(vk & 0xFF) == 0);	 // We always send keydown, but only last keyup is sent.
	}
	INPUT input{};
	input.type			 = INPUT_KEYBOARD;
	input.ki.wVk		 = vk;
	input.ki.wScan		 = scan;
	input.ki.dwFlags	 = bDown ? 0 : KEYEVENTF_KEYUP;
	input.ki.time		 = 0;
	input.ki.dwExtraInfo = 0;
	m_input.emplace_back(input);
}

const std::array<CSendInput::KEYMAP, 112>& CSendInput::GetKeymap() noexcept
{
	static constexpr std::array<CSendInput::KEYMAP, 112> keymap{ {
		{ _T("BACK"), VK_BACK },
		{ _T("TAB"), VK_TAB },
		{ _T("CLEAR"), VK_CLEAR },
		{ _T("RETURN"), VK_RETURN },
		{ _T("SHIFT"), VK_SHIFT },
		{ _T("CTRL"), VK_CONTROL },
		{ _T("CONTROL"), VK_CONTROL },
		{ _T("MENU"), VK_MENU },
		{ _T("PAUSE"), VK_PAUSE },
		{ _T("CAPITAL"), VK_CAPITAL },
		{ _T("KANA"), VK_KANA },
		{ _T("HANGEUL"), VK_HANGEUL },
		{ _T("HANGUL"), VK_HANGUL },
		{ _T("JUNJA"), VK_JUNJA },
		{ _T("FINAL"), VK_FINAL },
		{ _T("HANJA"), VK_HANJA },
		{ _T("KANJI"), VK_KANJI },
		{ _T("ESC"), VK_ESCAPE },
		{ _T("ESCAPE"), VK_ESCAPE },
		{ _T("CONVERT"), VK_CONVERT },
		{ _T("NONCONVERT"), VK_NONCONVERT },
		{ _T("ACCEPT"), VK_ACCEPT },
		{ _T("MODECHANGE"), VK_MODECHANGE },
		{ _T("PRIOR"), VK_PRIOR },
		{ _T("NEXT"), VK_NEXT },
		{ _T("END"), VK_END },
		{ _T("HOME"), VK_HOME },
		{ _T("LEFT"), VK_LEFT },
		{ _T("UP"), VK_UP },
		{ _T("RIGHT"), VK_RIGHT },
		{ _T("DOWN"), VK_DOWN },
		{ _T("SELECT"), VK_SELECT },
		{ _T("PRINT"), VK_PRINT },
		{ _T("EXECUTE"), VK_EXECUTE },
		{ _T("SNAPSHOT"), VK_SNAPSHOT },
		{ _T("INS"), VK_INSERT },
		{ _T("INSERT"), VK_INSERT },
		{ _T("DEL"), VK_DELETE },
		{ _T("DELETE"), VK_DELETE },
		{ _T("HELP"), VK_HELP },
		{ _T("LWIN"), VK_LWIN },
		{ _T("RWIN"), VK_RWIN },
		{ _T("APPS"), VK_APPS },
		{ _T("SLEEP"), VK_SLEEP },
		{ _T("NUMPAD0"), VK_NUMPAD0 },
		{ _T("NUMPAD1"), VK_NUMPAD1 },
		{ _T("NUMPAD2"), VK_NUMPAD2 },
		{ _T("NUMPAD3"), VK_NUMPAD3 },
		{ _T("NUMPAD4"), VK_NUMPAD4 },
		{ _T("NUMPAD5"), VK_NUMPAD5 },
		{ _T("NUMPAD6"), VK_NUMPAD6 },
		{ _T("NUMPAD7"), VK_NUMPAD7 },
		{ _T("NUMPAD8"), VK_NUMPAD8 },
		{ _T("NUMPAD9"), VK_NUMPAD9 },
		{ _T("MULTIPLY"), VK_MULTIPLY },
		{ _T("ADD"), VK_ADD },
		{ _T("SEPARATOR"), VK_SEPARATOR },
		{ _T("SUBTRACT"), VK_SUBTRACT },
		{ _T("DECIMAL"), VK_DECIMAL },
		{ _T("DIVIDE"), VK_DIVIDE },
		{ _T("F1"), VK_F1 },
		{ _T("F2"), VK_F2 },
		{ _T("F3"), VK_F3 },
		{ _T("F4"), VK_F4 },
		{ _T("F5"), VK_F5 },
		{ _T("F6"), VK_F6 },
		{ _T("F7"), VK_F7 },
		{ _T("F8"), VK_F8 },
		{ _T("F9"), VK_F9 },
		{ _T("F10"), VK_F10 },
		{ _T("F11"), VK_F11 },
		{ _T("F12"), VK_F12 },
		{ _T("F13"), VK_F13 },
		{ _T("F14"), VK_F14 },
		{ _T("F15"), VK_F15 },
		{ _T("F16"), VK_F16 },
		{ _T("F17"), VK_F17 },
		{ _T("F18"), VK_F18 },
		{ _T("F19"), VK_F19 },
		{ _T("F20"), VK_F20 },
		{ _T("F21"), VK_F21 },
		{ _T("F22"), VK_F22 },
		{ _T("F23"), VK_F23 },
		{ _T("F24"), VK_F24 },
		{ _T("NUMLOCK"), VK_NUMLOCK },
		{ _T("SCROLL"), VK_SCROLL },
		{ _T("LSHIFT"), VK_LSHIFT },
		{ _T("RSHIFT"), VK_RSHIFT },
		{ _T("LCTRL"), VK_LCONTROL },
		{ _T("LCONTROL"), VK_LCONTROL },
		{ _T("RCTRL"), VK_RCONTROL },
		{ _T("RCONTROL"), VK_RCONTROL },
		{ _T("LMENU"), VK_LMENU },
		{ _T("RMENU"), VK_RMENU },
		{ _T("BROWSER_BACK"), VK_BROWSER_BACK },
		{ _T("BROWSER_FORWARD"), VK_BROWSER_FORWARD },
		{ _T("BROWSER_REFRESH"), VK_BROWSER_REFRESH },
		{ _T("BROWSER_STOP"), VK_BROWSER_STOP },
		{ _T("BROWSER_SEARCH"), VK_BROWSER_SEARCH },
		{ _T("BROWSER_FAVORITES"), VK_BROWSER_FAVORITES },
		{ _T("BROWSER_HOME"), VK_BROWSER_HOME },
		{ _T("VOLUME_MUTE"), VK_VOLUME_MUTE },
		{ _T("VOLUME_DOWN"), VK_VOLUME_DOWN },
		{ _T("VOLUME_UP"), VK_VOLUME_UP },
		{ _T("MEDIA_NEXT_TRACK"), VK_MEDIA_NEXT_TRACK },
		{ _T("MEDIA_PREV_TRACK"), VK_MEDIA_PREV_TRACK },
		{ _T("MEDIA_STOP"), VK_MEDIA_STOP },
		{ _T("MEDIA_PLAY_PAUSE"), VK_MEDIA_PLAY_PAUSE },
		{ _T("LAUNCH_MAIL"), VK_LAUNCH_MAIL },
		{ _T("LAUNCH_MEDIA_SELECT"), VK_LAUNCH_MEDIA_SELECT },
		{ _T("LAUNCH_APP1"), VK_LAUNCH_APP1 },
		{ _T("LAUNCH_APP2"), VK_LAUNCH_APP2 },
	} };
	return keymap;
}
