module;

#define NOMINMAX

#include <array>
#include <vector>

#include <tchar.h>
#include <windows.h>

export module SendInput;

export class CSendInput {
public:
	explicit CSendInput(HKL hkl);
	CSendInput(const CSendInput&) = delete;
	CSendInput(CSendInput&&)	  = delete;
	CSendInput& operator=(const CSendInput&) = delete;
	CSendInput& operator=(CSendInput&&) = delete;
	~CSendInput() noexcept				= default;

	enum class eError { Success,
						Syntax,
						Error };
	eError Add(LPCTSTR str);

	void Send() noexcept;

	struct KEYMAP {
		LPCTSTR name;
		SHORT vk;
	};
	static const std::array<CSendInput::KEYMAP, 112>& GetKeymap() noexcept;

protected:
	eError AddStr(LPCTSTR str);
	void AddInputChar(TCHAR c, bool bDown, bool bUp);
	void AddInputVk(SHORT vk, bool bDown, bool bUp);
	void AddInput(WORD vk, WORD scan, bool bDown);

	// Keyboard layout for the target window.
	const HKL m_hkl;

	// Generated input.
	std::vector<INPUT> m_input;

	// For each key (scancode is index into array) gives the number of times a key down was sent.
	std::array<std::size_t, 256> m_countKeyDown;
};
